# Написати програму, яка визначає, що звір перебуває перед нами. Якщо звір каже “Meow”, це кішка, якщо “Bark”,
# це собака, і якщо щось інше, це “Невідомий звір”.

sound = input("say! ").lower()

if sound == "meow":
    print("It is cat!")
elif sound == "bark":
    print("It is dog!")
else:
    print("It is unrecognized animal!")